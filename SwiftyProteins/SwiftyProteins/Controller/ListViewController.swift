//
//  ListViewController.swift
//  proteinsPart
//
//  Created by Ivan Zelenskyi on 12/6/18.
//  Copyright © 2018 Ivan Zelenskyi. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {

    @IBOutlet weak var searchTable: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var emptyView: UIView!
    
    
    var proteins = ProteinsData()
    var tableLoader = ImageLoadingWithCache()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        proteins.loadLigands()
        print(proteins.count())
        searchTable.rowHeight = 100
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SearchToProteins" {
            if let proteinsViewController: SceneVC = segue.destination as? SceneVC {
                let package = (sender as! (targetLigand : String, data : (atoms : [String], connections : [String])))
                let targetLigand = package.targetLigand
                let data = package.data
                proteinsViewController.name = targetLigand
                proteinsViewController.atoms = data.atoms
                proteinsViewController.connections = data.connections
            }
        }
    }
}
