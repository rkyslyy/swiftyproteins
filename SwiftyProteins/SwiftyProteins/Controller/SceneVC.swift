//
//  ViewController.swift
//  SwiftyProteins
//
//  Created by Roman Kyslyy on 11/17/18.
//  Copyright © 2018 Roman Kyslyy. All rights reserved.
//

import UIKit
import SceneKit

class SceneVC: UIViewController {

    var scnView : SCNView!
    var scene : ProteinScene!
    var atoms : [String]!
    var connections : [String]!
    var name : String!
    
    var rotating = false
    var hHidden = false
    var tHidden = true
    var rad = 0.2
    
    @IBOutlet weak var ligandName: UILabel!
    @IBOutlet weak var backButton : UIButton!
    @IBOutlet weak var shareButton : UIButton!
    @IBOutlet weak var rotatingStatus : UILabel!
    @IBOutlet weak var hHiddenStatus : UILabel!
    @IBOutlet weak var tHiddenStatus : UILabel!
    @IBOutlet weak var buttonsStack : UIStackView!
    @IBOutlet weak var logo : UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(tap:)))
        
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func handleTap(tap : UITapGestureRecognizer) {
        let proteinScene = (scnView.scene! as! ProteinScene)
        if proteinScene.allTitlesVisible {
            return
        }
        let atomsInScene = proteinScene.atoms
        let location = tap.location(in: scnView)
        let atomsInLocation = scnView.hitTest(location, options: nil)
        
        if proteinScene.selectedAtomTitleVisible != nil {
            let fadeOut = SCNAction.fadeOut(duration: 0.15)
            proteinScene.selectedAtomTitleVisible!.runAction(fadeOut, completionHandler: {
                proteinScene.selectedAtomTitleVisible = nil
                
                self.showSelectedTitle(atomsInLocation: atomsInLocation,
                                  atomsInScene: atomsInScene,
                                  proteinScene: proteinScene)
            })
        } else {
            showSelectedTitle(atomsInLocation: atomsInLocation,
                              atomsInScene: atomsInScene,
                              proteinScene: proteinScene)
        }
    }
    
    private func showSelectedTitle(atomsInLocation : [SCNHitTestResult],
                                   atomsInScene : [Atom],
                                   proteinScene : ProteinScene) {
        for atomInLocation in atomsInLocation {
            if atomInLocation.node.geometry is SCNSphere {
                let atomNode = atomInLocation.node
                for atom in atomsInScene {
                    if atom.atomNode === atomNode && !atom.atomNode.isHidden {
                        let fadeIn = SCNAction.fadeIn(duration: 0.15)
                        atom.titleNode.runAction(fadeIn)
                        proteinScene.selectedAtomTitleVisible = atom.titleNode
                    }
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        buttonsStack.alpha = 0
        backButton.alpha = 0
        shareButton.alpha = 0
        ligandName.alpha = 0
        ligandName.text = name.uppercased()
    }

    override func viewDidAppear(_ animated: Bool) {
        scnView.isUserInteractionEnabled = false
        scnView.scene = ProteinScene(atoms: atoms, connections: connections)
        scnView.scene?.rootNode.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            UIView.animate(withDuration: 0.5, animations: {
                self.logo.alpha = 0
                self.spinner.alpha = 0
            }, completion: { (_) in
                self.logo.isHidden = true
                self.spinner.isHidden = true
                self.scnView.alpha = 0
                self.scnView.scene?.rootNode.isHidden = false
                self.buttonsStack.alpha = 1
                self.backButton.alpha = 1
                self.shareButton.alpha = 1
                self.ligandName.alpha = 1
                self.scnView.isUserInteractionEnabled = true
                UIView.animate(withDuration: 1, animations: {
                    self.scnView.scene?.background.contents = UIColor.darkGray
                    self.scnView.alpha = 1
                })
            })
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.scene = nil
    }

    private func initView() {
        scnView = (self.view as! SCNView)
        scnView.backgroundColor = .black
        scnView.autoenablesDefaultLighting = true
        scnView.isUserInteractionEnabled = true
        scnView.allowsCameraControl = true
        spinner.startAnimating()
    }
    
    @IBAction func goBack() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func rotateDisBitch(rad : Double) {
        if !rotating {
            return
        }
        scnView.scene?.rootNode.runAction(SCNAction.rotate(toAxisAngle: SCNVector4(0, 0.1, 0, rad),
                                                           duration: 0.4),
                                          completionHandler: {
            DispatchQueue.main.async {
                self.rad += 0.2
                self.rotateDisBitch(rad : self.rad)
            }
        })
    }
    
    @IBAction func toggleRotation() {
        rotating = !rotating
        rotatingStatus.textColor = rotating ? UIColor.green : UIColor.red
        rotatingStatus.text = rotating ? "ON" : "OFF"
        rotateDisBitch(rad : rad)
    }
    
    @IBAction func toggleHydrogen() {
        hHidden = !hHidden
        hHiddenStatus.textColor = hHidden ? UIColor.red : UIColor.green
        hHiddenStatus.text = hHidden ? "HIDDEN" : "SHOWN"
        (scnView.scene as! ProteinScene).toggleHydrogen(hide: hHidden)
    }
    
    @IBAction func toggleTitles() {
        tHidden = !tHidden
        tHiddenStatus.textColor = tHidden ? UIColor.red : UIColor.green
        tHiddenStatus.text = tHidden ? "HIDDEN" : "SHOWN"
        (scnView.scene as! ProteinScene).toggleTitles(hide: tHidden)
    }
    
    @IBAction func share() {
        let image = self.scnView.snapshot()
        DispatchQueue.main.async {
            let activityVC = UIActivityViewController(activityItems: [image], applicationActivities: nil)
            activityVC.popoverPresentationController?.sourceView = self.view
            
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
