//
//  LoginViewController.swift
//  proteinsPart
//
//  Created by Ivan Zelenskyi on 12/6/18.
//  Copyright © 2018 Ivan Zelenskyi. All rights reserved.
//

import UIKit
import LocalAuthentication

class LoginViewController: UIViewController {

    let context: LAContext = LAContext()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        let context = LAContext()
        var error : NSError?
        if #available(iOS 8.0, macOS 10.12.1, *) {
            if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
                context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: "It's just security check"){
                    success, evaluateError in
                    DispatchQueue.main.async {
                        if success {
                            self.performSegue(withIdentifier: "LoginToSearch", sender: sender)
                            print("Success")
                        } else {
                            print("Failed")
                        }
                    }
                }
            } else {
                print(error ?? "empty")
            }
        } else {
            print("Not supported")
        }
    }
}
