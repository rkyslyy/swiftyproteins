//
//  Atom.swift
//  SwiftyProteins
//
//  Created by Roman Kyslyy on 11/19/18.
//  Copyright © 2018 Roman Kyslyy. All rights reserved.
//

import Foundation
import SceneKit

struct Atom {
    let atomNode : SCNNode
    let titleNode : SCNNode
    let formula : String
    let elementLetter : String
    
    init(atomNode : SCNNode, titleNode : SCNNode, formula : String) {
        self.atomNode = atomNode
        self.titleNode = titleNode
        self.formula = formula
        self.elementLetter = String(formula.prefix(1))
    }
}
