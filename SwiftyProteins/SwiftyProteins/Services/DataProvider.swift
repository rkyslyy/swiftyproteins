//
//  DataProvider.swift
//  SwiftyProteins
//
//  Created by Roman Kyslyy on 11/17/18.
//  Copyright © 2018 Roman Kyslyy. All rights reserved.
//

import Foundation
import UIKit

class DataProvider {
    
    class func getData(ligandName : String) ->
        (atoms : [String], connections : [String])? {
        let firstLetter = ligandName.prefix(1)
        guard let lines = getFileContents(url: "http://files.rcsb.org/ligands/" +
            "\(firstLetter)/\(ligandName)/\(ligandName)_ideal.pdb")
            else { print("Could not get contents") ; return nil }
            if lines.isEmpty || lines.count == 1 {
                return nil
            }
        let atoms = getAtomsLines(lines: lines)
        let connections = getConnectionsLines(lines: lines)
        return (atoms : atoms, connections : connections)
    }
    
    //Get file contents
    class private func getFileContents(url : String) -> [String]? {
        guard let data = try? Data(contentsOf: URL(string: url)!) else { return nil }
        guard let str = String(data: data, encoding: String.Encoding.utf8) else { return nil }
        var lines = str.components(separatedBy: "\n")
        for (index, line) in lines.enumerated() {
            lines[index] = line.replacingOccurrences(of: "  ", with: " ")
                .replacingOccurrences(of: "  ", with: " ")
                .replacingOccurrences(of: "  ", with: " ")
                .replacingOccurrences(of: "  ", with: " ")
        }
        return lines
    }
    
    //Separate atoms and connections
    class private func getAtomsLines(lines : [String]) -> [String] {
        var atoms = [String]()
        for line in lines {
            let parts = line.components(separatedBy: " ")
            if parts[0] == "ATOM" {
                atoms.append(line)
            }
        }
        return atoms
    }
    
    class private func getConnectionsLines(lines : [String]) -> [String] {
        var connections = [String]()
        for line in lines {
            let parts = line.components(separatedBy: " ")
            if parts[0] == "CONECT" {
                connections.append(line)
            }
        }
        return connections
    }
    
    class func getColor(forAtom atom : String) -> UIColor {
        switch atom {
        case "H" : return UIColor.white
        case "N" : return UIColor.blue
        case "C" : return UIColor.gray
        case "O" : return UIColor.red
        case "P" : return UIColor.orange
        case "F", "Cl" : return UIColor.green
        case "Br" : return UIColor.brown
        case "I" : return UIColor.purple
        case "He", "Ne", "Ar", "Xe", "Kr" : return UIColor.cyan
        case "S" : return UIColor.yellow
        case "B" : return UIColor(red: 253 / 255,
                                  green: 153 / 255,
                                  blue: 101 / 255,
                                  alpha: 1)
        case "Li", "Na", "K", "Rb", "Cs" : return UIColor(red: 97 / 255,
                                                          green: 0,
                                                          blue: 254 / 255,
                                                          alpha: 1)
        case "Be", "Mg", "Ca", "Sr", "Ba", "Ra" : return UIColor(red: 13 / 255,
                                                                 green: 103 / 255,
                                                                 blue: 3 / 255,
                                                                 alpha: 1)
        case "Ti" : return UIColor.gray
        case "Fe" : return UIColor(red: 211 / 255,
                                   green: 98 / 255,
                                   blue: 8 / 255,
                                   alpha: 1)
        default : return UIColor(red: 211 / 255,
                                 green: 87 / 255,
                                 blue: 1,
                                 alpha: 1)
        }
    }
}
