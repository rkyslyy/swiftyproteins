//
//  ProteinsData.swift
//  proteinsPart
//
//  Created by Ivan Zelenskyi on 12/6/18.
//  Copyright © 2018 Ivan Zelenskyi. All rights reserved.
//

import Foundation

class ProteinsData
{
    var proteins = [String]()
    var filteredProteins = [String]()

    func loadLigands(){
        if let path = Bundle.main.path(forResource: "ligands", ofType: "txt"){
            do{
                let data = try String(contentsOfFile: path, encoding: .utf8)
                proteins = data.components(separatedBy: .newlines)
                filteredProteins = proteins
            } catch {
                fatalError("Loading Data Error")
            }
        }
    }

    func filterByText(text: String){
        if text == "" {
            filteredProteins = proteins
        } else {
            filteredProteins = proteins.filter { $0.contains(text)}
        }
    }
    
    func count() -> Int{
        return filteredProteins.count
    }
    
}
