//
//  ProteinScene.swift
//  SwiftyProteins
//
//  Created by Roman Kyslyy on 11/17/18.
//  Copyright © 2018 Roman Kyslyy. All rights reserved.
//

import UIKit
import SceneKit

class ProteinScene: SCNScene {

    var atoms = [Atom]()
    var hConnections = [CylinderLine]()
    var hVisible = true
    var allTitlesVisible = false
    var selectedAtomTitleVisible : SCNNode?
    
    init(atoms : [String], connections : [String]) {
        super.init()
        
        createAtoms(atoms: atoms)
        createConnections(connections: connections)
        createTitles()
    }
    
    //Create atoms and get vectors
    private func createAtoms(atoms : [String]) {
        for atom in atoms {
            let parts = atom.components(separatedBy: " ")
            let x = Double(parts[6])! * 64
            let y = Double(parts[7])! * 64
            let z = Double(parts[8])! * 64

            let atomNode = generateAtomNode(stringParts: parts, x: x, y: y, z: z)
            rootNode.addChildNode(atomNode)

            let titleNode = generateTitleNode(formula: parts[2], x: x, y: y, z: z)

            self.atoms.append(Atom(atomNode : atomNode,
                                   titleNode : titleNode,
                                   formula : parts[2]))
        }
    }
    //-------------------------------------------------------------------------
    
    private func generateAtomNode(stringParts : [String],
                                  x : Double,
                                  y : Double,
                                  z : Double) -> SCNNode {
        let geometry = SCNSphere(radius: 16)
        geometry.firstMaterial?.diffuse.contents = DataProvider.getColor(forAtom: stringParts[11])
        let node = SCNNode(geometry: geometry)
        let vector = SCNVector3(x, y, z)
        node.position = vector
        return node
    }
    
    private func generateTitleNode(formula : String,
                                   x : Double,
                                   y : Double,
                                   z : Double) -> SCNNode {
        let chemistryString = getChemistryString(formula: formula)
        let title = SCNText(string: chemistryString, extrusionDepth: 0.1)
        let titleNode = SCNNode(geometry: title)
        titleNode.position = SCNVector3(x, y + 13, z)
        return titleNode
    }
    
    private func getChemistryString(formula : String) -> NSMutableAttributedString {
        let font = UIFont(name: "Avenir", size: 15)
        let chemistryString = NSMutableAttributedString(string: "",
                                            attributes: [NSAttributedStringKey.font : font!])
        for character in formula.uppercased().unicodeScalars {
            if CharacterSet.decimalDigits.contains(character) {
                let underFont = UIFont(name: "Avenir", size: 7)
                chemistryString.append(NSAttributedString(string: String(character),
                                              attributes: [.font : underFont!]))
            } else {
                chemistryString.append(NSAttributedString(string: String(character),
                                              attributes: [.font : font!]))
            }
        }
        return chemistryString
    }
    
    //Create connections
    private func createConnections(connections : [String]) {
        var connectionDic = [Int : [Int]]()
        
        for connection in connections {
            var parts = connection.components(separatedBy: " ")
            parts.remove(at: 0)
            connectionDic[Int(parts[0])!] = [Int]()
            if Int(parts[0])! > atoms.count {
                continue
            }
            let from = atoms[Int(parts[0])! - 1].atomNode.position
            for vectorIndex in 1..<parts.count {
                if Int(parts[vectorIndex])! > atoms.count {
                    continue
                }
                if connectionDic[Int(parts[vectorIndex])!] != nil {
                    if connectionDic[Int(parts[vectorIndex])!]!.contains(Int(parts[0])!) {
                        continue
                    }
                }
                connectionDic[Int(parts[0])!]?.append(Int(parts[vectorIndex])!)
                let to = atoms[Int(parts[vectorIndex])! - 1].atomNode.position
                let line = CylinderLine(parent: rootNode,
                                        v1: from,
                                        v2: to,
                                        radius: 2.5,
                                        radSegmentCount: 100,
                                        color: .white)
                if atoms[Int(parts[0])! - 1].elementLetter == "H" ||
                    atoms[Int(parts[vectorIndex])! - 1].elementLetter == "H" {
                    hConnections.append(line)
                }
                rootNode.addChildNode(line)
            }
        }
    }
    //----------------------------------------------------------
    
    //Create titles
    private func createTitles() {
        for atom in atoms {
            let title = atom.titleNode
            let con = SCNBillboardConstraint()
            title.constraints = [con]
            rootNode.addChildNode(title)
            let fadeOut = SCNAction.fadeOut(duration: 0)
            atom.titleNode.runAction(fadeOut)
        }
    }
    
    //Toggle hydrogen
    func toggleHydrogen(hide state : Bool) {
        hVisible = !state
        let fadeOut = SCNAction.fadeOut(duration: 0.1)
        let fadeIn = SCNAction.fadeIn(duration: 0.1)
        for con in hConnections {
            if state {
                con.runAction(fadeOut)
            } else {
                con.runAction(fadeIn)
            }
        }
        for atom in atoms {
            if atom.elementLetter == "H" {
                if state {
                    atom.atomNode.runAction(fadeOut)
                } else {
                    atom.atomNode.runAction(fadeIn)
                }
                if state == false && allTitlesVisible == false {
                    continue
                }
                if state {
                    atom.titleNode.runAction(fadeOut)
                } else {
                    atom.titleNode.runAction(fadeIn)
                }
            }
        }
    }

    //Toggle titles
    func toggleTitles(hide state : Bool) {
        allTitlesVisible = !state
        for atom in atoms {
            if state == false && atom.elementLetter == "H" && hVisible == false {
                continue
            }
            if state == true {
                let fadeOut = SCNAction.fadeOut(duration: 0.1)
                atom.titleNode.runAction(fadeOut)
            } else {
                let fadeIn = SCNAction.fadeIn(duration: 0.1)
                atom.titleNode.runAction(fadeIn)
            }
        }
    }

    //Trash------------------------------------------------
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    //-----------------------------------------------------
    
}
