//
//  CustomPopup.swift
//  SwiftyProteins
//
//  Created by Roman KYSLYY on 12/11/18.
//  Copyright © 2018 Roman KYSLYY. All rights reserved.
//

import UIKit

class CustomPopup: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var errorMessage: UILabel!
    @IBOutlet weak var OKButton: UIButton!
    
    var parentController : UIViewController!
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    convenience init(frame : CGRect, controller : UIViewController) {
        self.init(frame: frame)
        self.parentController = controller
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }
    
    private func customInit() {
        Bundle.main.loadNibNamed("CustomPopupXIB", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor(red: 245 / 255,
                                         green: 66 / 255,
                                         blue: 133 / 255,
                                         alpha: 1).cgColor
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.shake()
    }

    @IBAction func OKPressed(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0
            self.transform = self.OKButton.transform.rotated(by: CGFloat(Double.pi))
            (self.parentController as! ListViewController).searchTable.layer.opacity = 1
        }) { (_) in
            (self.parentController as! ListViewController).searchTable.isUserInteractionEnabled = true
            (self.parentController as! ListViewController).searchBar.isUserInteractionEnabled = true
            self.removeFromSuperview()
        }
    }
}

extension UIView {
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
}

