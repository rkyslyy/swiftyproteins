//
//  SearchTableExtension.swift
//  proteinsPart
//
//  Created by Ivan Zelenskyi on 12/6/18.
//  Copyright © 2018 Ivan Zelenskyi. All rights reserved.
//

import UIKit

extension   ListViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return proteins.count()
    }
    //https://files.rcsb.org/ligands/6/607/607-100.gif
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = searchTable.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? SearchTableCell else{
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        cell.proteinName.text = proteins.filteredProteins[indexPath.row]
        tableLoader.getProteinImage(name: proteins.filteredProteins[indexPath.row], imageView: cell.proteinImage, defaultImage: "default")
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = self.searchTable.indexPathForSelectedRow
        let targetLigand = self.proteins.filteredProteins[indexPath!.row]
        guard let data = DataProvider.getData(ligandName: targetLigand)
            else { return self.presentError(errorMessage: "Could not load 3D-model from rcsb.org") }
        performSegue(withIdentifier: "SearchToProteins", sender: (targetLigand : targetLigand, data : data))
    }
    
    func presentError(errorMessage : String) {
        UIView.animate(withDuration: 0.3) {
            self.searchTable.layer.opacity = 0.3
        }
        let view = CustomPopup(frame: CGRect(origin: self.view.center, size: CGSize(width: 230, height: 120)), controller: self)
        
        view.translatesAutoresizingMaskIntoConstraints = true
        view.center = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)
        view.autoresizingMask = [UIViewAutoresizing.flexibleLeftMargin,
                                 UIViewAutoresizing.flexibleRightMargin,
                                 UIViewAutoresizing.flexibleTopMargin,
                                 UIViewAutoresizing.flexibleBottomMargin]
        
        view.backgroundColor = .red
        view.errorMessage.text = errorMessage
        self.view.addSubview(view)
        searchTable.isUserInteractionEnabled = false
        searchBar.isUserInteractionEnabled = false
    }
}
