//
//  SearchBarExtension.swift
//  proteinsPart
//
//  Created by Ivan Zelenskyi on 12/6/18.
//  Copyright © 2018 Ivan Zelenskyi. All rights reserved.
//

import UIKit
extension   ListViewController : UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        proteins.filterByText(text: searchText.uppercased())
        searchTable.reloadData()
        if (searchTable.numberOfRows(inSection: 0) == 0){
            emptyView.isHidden = false
        } else {
            emptyView.isHidden = true
        }
    }
}
