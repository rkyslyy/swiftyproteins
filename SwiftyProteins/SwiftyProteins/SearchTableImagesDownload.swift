//
//  SearchTableImagesDownload.swift
//  proteinsPart
//
//  Created by Ivan Zelenskyi on 12/7/18.
//  Copyright © 2018 Ivan Zelenskyi. All rights reserved.
//

import Foundation
import UIKit

class ImageLoadingWithCache {

    var imageCache = [String:UIImage]()
    
    func getProteinImage(name: String, imageView: UIImageView, defaultImage: String) {
        if let img = imageCache[name] {
            imageView.image = img
        } else {
            guard let url = URL(string: "https://files.rcsb.org/ligands/\(name.prefix(1))/\(name)/\(name)-100.gif") else { return }
            let request: URLRequest = URLRequest(url: url)
            
            URLSession.shared.dataTask(with: request, completionHandler: {
                (data, response, error) in
                    if error == nil {
                        if let image = UIImage(data: data!){
                            self.imageCache[name] = image
                            DispatchQueue.main.async{
                                imageView.image = image
                            }
                        } else{
                            DispatchQueue.main.async{
                                imageView.image = UIImage(named: "empty")
                            }
                        }
                    } else {
                        print("image not found")
                        imageView.image = UIImage(named: "empty")
                }
            }).resume()
        }
    }
    
}
